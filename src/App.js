import React from "react";
import { BrowserRouter, NavLink, Switch, Route } from "react-router-dom";
import { Container} from "reactstrap";


// importación de componentes para las rutas
import Detall from "./components/Detall";
import Nova from "./components/Nova";
import Home from "./components/Home";
import Llista from "./components/Llista";
import Registre from "./components/Registrar";
import Login from "./components/Login";
import Logout from "./components/Logout";

// componente APP
const App = () => {

  return (
      <BrowserRouter>
        <Container>
          <br />
          <ul className="nav nav-tabs">
          <li className="nav-item">
              <NavLink exact className="nav-link" to="/">
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/registre">
                Registre
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/login">
               Login
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/logout">
               Logout
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/llista">
                Llista
              </NavLink>
            </li>
          </ul>
          <br />
          <br />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/llista" component={Llista} />
            <Route path="/foto/:id" component={Detall} />
            <Route path="/nova" component={Nova} />
            <Route path="/login" component={Login} />
            <Route path="/logout" component={Logout} />
            <Route path="/registre" component={Registre} />
          </Switch>
        </Container>
      </BrowserRouter>
  );
};

export default App;
