import React, {useState, useEffect} from "react";
import {Redirect, Link} from "react-router-dom";
import Controller from '../controllers/FotosController';

const API_FOTOS = 'http://localhost:3003/img/';

const Detall = (props) => {

  const [foto, setFoto] = useState({});

  useEffect(()=>{
    const id = props.match.params.id;
    Controller.getById(id)
    .then(data => setFoto(data))
    .catch(err => {
      return <Redirect to="/" />;
    });
  }, [props.match.params.id])


  return (
    <>
      <h3>Foto</h3>
      <hr />
      <img src={API_FOTOS+foto.urlfoto} width="300"/>
      <hr />
      <h3>Titol: {foto.titulo}</h3>
      <p>Comentari: {foto.comentario}</p>
      <h2>Likes: {foto.likes}</h2>
      <hr />
      <Link className='btn btn-primary' to='/' >Torna</Link>
    </>
  );
};


export default Detall;