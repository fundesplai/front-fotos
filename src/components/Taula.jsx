import React from "react";
import { Table } from "reactstrap";
import {
  Link,
} from "react-router-dom";

const Taula = (props) => {

  const filas = props.datos.map((el) => (
    <tr key={el.id}>
      {props.columnas.map((col, idx) => <td key={idx}>{el[col['nom']]}</td>)}
      <td><Link className="btn btn-success btn-sm" to={props.rutaShow + el.id} >Detall</Link></td>
    </tr>
  ));

  return (
    <>

    <Table striped>
      <thead>
        <tr>
          {props.columnas.map((el, idx) => <th key={idx}>{el['titol']}</th>)}
          <th></th>
          <th></th>
          <th></th>
        </tr>
      
      </thead>
      <tbody>{filas}</tbody>
      <tfoot></tfoot>
    </Table>
    </>
  );
};

export default Taula;
