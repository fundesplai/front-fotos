import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";

import { Button, Row, Col, FormGroup, Label, Input } from "reactstrap";
import Controller from "../controllers/UsuariController";


// anterior xmysql
// xmysql -h localhost -d agenda -u root

const EditaFotoContacte = (props) => {
  const [tornar, setTornar] = useState(false);
 
  const [selectedFile, setSelectedFile] = useState(false);
  const [titol, setTitol] = useState("");
  const [comentari, setComentari] = useState("");

  const enviarFoto = () => {
    const data = new FormData();
    data.append("file", selectedFile);
    data.append("titulo", titol);
    data.append("comentario", comentari);

    Controller.novaFoto(data)
    .then(() => setTornar(true));
   
  };

  if (tornar) {
    return <Redirect to="/" />;
  }

  return (
    <>
      <br />
      <Row>
        <Col>
          <h1>Nova foto</h1>
        </Col>
        <Col>
          <span className="float-right">
            <Button
              type="button"
              onClick={() => setTornar(true)}
              size="sm"
              color="danger"
            >
              {"Sortir sense desar"}
            </Button>{" "}
            <Button onClick={enviarFoto} size="sm" color="success">
              {"Desar canvis"}
            </Button>
          </span>
        </Col>
      </Row>
      <br />

      <Row>
        <Col sm="8">
          <FormGroup>
            <Input
              type="file"
              onChange={(e) => setSelectedFile(e.target.files[0])}
            />
          </FormGroup>

          <FormGroup>
            <Label for="titol">Titol</Label>
            <Input
              type="text"
              id="titol"
              value={titol}
              onChange={(e) => setTitol(e.target.value)}
            />
          </FormGroup>

          <FormGroup>
            <Label for="comentari">Comentari</Label>
            <Input
              type="text"
              id="comentari"
              value={comentari}
              onChange={(e) => setComentari(e.target.value)}
            />
          </FormGroup>
        </Col>
      </Row>
    </>
  );
};

export default EditaFotoContacte;
