import React from "react";
import { withCookies } from "react-cookie";

const Home = (props) => {
  return (
    <>
      <br />

      <h2>Home</h2>
      <h5>Nom a cookies: {props.cookies.get("nom") || "---"}</h5>
      <h5>Token cookies: {props.cookies.get("token") || "---"}</h5>
    </>
  );
};

export default withCookies(Home);
