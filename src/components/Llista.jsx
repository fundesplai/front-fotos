import React, {  useEffect, useState } from "react";
import Controller from "../controllers/FotosController";
import { Link } from "react-router-dom";
import { withCookies } from "react-cookie";

import { Row, Col } from "reactstrap";

import Taula from "./Taula";

const Llista =  (props) => {
  const token = props.cookies.get("token");
  const [dades, setDades] = useState([]);
  const [error, setError] = useState('');

  useEffect(()=>{
    Controller.getAll(token)
     .then(data =>{
       if (data.ok){
         setDades(data.data);
       } else {
        setError(data.err);
       }
      })
     .catch(err => setError(err.message));
   }, [])


  const columnes = [
    {
      nom: "titulo",
      titol: 'Titol',
    },
    {
      nom: "comentario",
      titol: "Comentario",
    },
    {
      nom: "urlfoto",
      titol: 'Foto',
    },
    {
      nom: "likes",
      titol: 'Likes',
    },
  ];



  return (
    <>

      <Row>
        <Col>
          <h3>Llista</h3>
        </Col>
        <Col className="text-right">
          <Link className="btn btn-primary btn-sm" to="/nova">
            Nova
            </Link>
        </Col>
      </Row>

      <Taula
        datos={dades}
        columnas={columnes}
        rutaShow="/foto/"
      />

      {error && <h3>Error: {error}</h3>}
    </>
  );
};


export default withCookies(Llista);